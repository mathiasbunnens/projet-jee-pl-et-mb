package junia.projetbl.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
//@JsonIgnoreProperties(value = {"games"})
public class Company extends GenericEntity{


    private String name;

    @Lob
    private String description;
    private String imageURL;

    @OneToMany(fetch=FetchType.EAGER ,mappedBy = "company")
    private List<Game> games;

    @ManyToMany(fetch=FetchType.EAGER ,mappedBy = "companies")
    private Set<User> users;

    public Company() {
    }

    //region Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    //endregion
}
