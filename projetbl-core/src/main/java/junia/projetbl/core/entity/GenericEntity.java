package junia.projetbl.core.entity;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }

}
