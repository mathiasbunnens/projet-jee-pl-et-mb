package junia.projetbl.core.entity;


import javax.persistence.*;
import java.util.List;

@Entity
public class Article extends GenericEntity{

    private String title;

    @Lob
    private String text;

    @ManyToOne
    private Game game;

    public Article(){
    }

    //region Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    //endregion
}
