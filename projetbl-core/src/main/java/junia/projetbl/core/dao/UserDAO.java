package junia.projetbl.core.dao;

import junia.projetbl.core.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserDAO extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.email=:email")
    User getUserByEmail(@Param("email") String userEmail);

    /*
    @Modifying
    @Query("UPDATE User u SET u.games = ?1 WHERE u.id = ?2")
    void setUserInfoById(String games, long userId);
*/

}
