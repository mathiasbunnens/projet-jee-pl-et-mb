package junia.projetbl.core.dao;

import junia.projetbl.core.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameDAO extends JpaRepository<Game, Long> {
}
