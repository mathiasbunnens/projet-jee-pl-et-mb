package junia.projetbl.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import junia.projetbl.core.entity.Company;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CompanyDAO extends JpaRepository<Company,Long> {

    @Query("SELECT DISTINCT c FROM Company c LEFT JOIN FETCH c.games WHERE c.id=:id")
    Company getOneWithGames(@Param("id") long companyId);

}
