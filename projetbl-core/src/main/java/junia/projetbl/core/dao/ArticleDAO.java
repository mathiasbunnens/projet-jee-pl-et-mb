package junia.projetbl.core.dao;

import junia.projetbl.core.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleDAO extends JpaRepository<Article, Long> {
}
