package junia.projetbl.core.service;


import junia.projetbl.core.dao.ArticleDAO;
import junia.projetbl.core.entity.Article;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;
import java.util.Optional;

//TODO Remplacer le Service en Named de tous les services ?
@Named
@Transactional
public class ArticleService {

    private ArticleDAO articleDAO;

    public ArticleService (ArticleDAO articleDAO) { this.articleDAO = articleDAO; }

    public void deleteAll() { articleDAO.deleteAll(); }

    public void save(final Article article) {articleDAO.save(article); }

    public List<Article> findAll() { return articleDAO.findAll(); }

    public Optional<Article> findArticleDetails(long articleId) { return articleDAO.findById(articleId); }
}
