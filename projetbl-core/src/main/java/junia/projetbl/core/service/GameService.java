package junia.projetbl.core.service;

import junia.projetbl.core.dao.GameDAO;
import junia.projetbl.core.entity.Game;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.List;
import java.util.Optional;

@Named
@Transactional
public class GameService {

    private GameDAO gameDAO;

    public GameService (GameDAO gameDAO) { this.gameDAO  =gameDAO; }

    public void deleteAll() { gameDAO.deleteAll(); }

    public void save (final Game game) { gameDAO.save(game); }

    public List<Game> findAll() { return gameDAO.findAll(); }

    //public Game findGameDetails(long gameId) { return gameDAO.getOne(gameId); }

    public Optional<Game> findGameDetails(long gameId) { return gameDAO.findById(gameId); }
}
