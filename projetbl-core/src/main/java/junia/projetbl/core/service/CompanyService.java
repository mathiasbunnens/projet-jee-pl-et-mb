package junia.projetbl.core.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import junia.projetbl.core.dao.CompanyDAO;
import junia.projetbl.core.entity.Company;

import javax.inject.Named;
import java.util.List;
import java.util.Optional;

@Named
@Transactional
public class CompanyService {

    private CompanyDAO companyDAO;

    public CompanyService(CompanyDAO companyDAO) {
        this.companyDAO = companyDAO;
    }

    public void deleteAll() {
        companyDAO.deleteAll();
    }

    public void save(final Company company) {
        companyDAO.save(company);
    }

    public List<Company> findAll() { return companyDAO.findAll(); }

    public Company findCompanyDetails0(long companyId) {
        return companyDAO.getOneWithGames(companyId);
    }

    public Optional<Company> findCompanyDetails(long companyId) { return companyDAO.findById(companyId); }
}
