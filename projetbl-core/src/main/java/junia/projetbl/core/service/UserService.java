package junia.projetbl.core.service;


import junia.projetbl.core.dao.UserDAO;
import junia.projetbl.core.entity.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.util.Optional;

@Named
@Transactional
public class UserService {

    private UserDAO userDAO;

    public UserService (UserDAO userDAO) { this.userDAO = userDAO; }

    public void deleteAll() { userDAO.deleteAll(); }

    public void save(final User user) {userDAO.save(user); }

    public Optional<User> findUserDetails(long userId) { return userDAO.findById(userId); }

    public User findUserByEmail(String userEmail) { return userDAO.getUserByEmail(userEmail); }

}
