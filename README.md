# Projet JEE - PL et MB

**Pierre Larghero & Mathias Bunnens**

1. Présentation du site 

Ce projet a pour but de regrouper l'actualité de jeux vidéos présents sur le marché.
La page d'accueil, Actualités, recense l'intégralité des articles enregistrés sur le site. Chaque article parle d'un jeu, et chaque jeu est édité par une compagnie. Une compagnie a évidemment le droit d'éditer plusieurs jeux et d'innonder le marché de jeux tous plus innonvants les uns que les autres tels que FIFA18, FIFA19, FIFA20 ou encore le très attendu FIFA21.
Il est également possible d'écrire plusieurs articles à propos du même jeu, car dans le cas contraire, personne n'aurait jamais été au courant du quatrième report de date de sortie de Cyberpunk2077.

Il est possible de se connecter via le bouton home tout en haut à droite. (email : toto@toto; mot de passe : toto)
Un utilisateur connecté peut suivre ses jeux favoris et/ou ses compagnies de jeux favorites. 
Ainsi fait il a accès aux différentes nouveautés sous forme de sroll line. Chaque actualité parle donc d'un jeu développé par une companie. L'utilisateur peut obtenir des détails sur le jeux en question ainsi que sur la compagnie.

Il peut aussi en rechercher de nouveaux pour s'abonner à plus de contenu via comme toujours son profil, un menu a gauche est reservé à cet effet.

Deux pages sont disponibles pour recenser tous les jeux et compagnies. La page Découverte est encore en beta, faute de moyens financiers (le temps, c'est de l'argent).


2. Mise en place du projet et base de données

Pour obtenir toutes les informations de la base de donnée, il est nécessaire d'activer un réseau local. Une base de donnée vide nommée "projet_bl" est indispensable. Afin de la remplir, il faut lancer la classe **Application** dans le module data. La base de donnée est alors complete.
Des utilisateurs ont été créer afin d'utiliser tout le potentiel de la page profil, comme par exemple toto : 
- email : toto@toto
- mot de passe : toto
La gestion des mots de passe n'est absoluement pas sécurisée, notre objectif était simplement de pouvoir afficher un fil d'actulité différent pour chaque utilisateur.

Toto a aimé 2 jeux et une companie, il récupère donc les actualités des 2 jeux et toutes les actualités liées au jeux de la companie suivies.



Pour la répartition des tâches dans le projet, Matthias s'est principalement occuppé du front et du remplissage de la base de données, et Pierre du back et du angularjs.