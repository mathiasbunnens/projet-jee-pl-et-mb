-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 05 nov. 2020 à 16:35
-- Version du serveur :  5.7.30
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données : `jeebdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `Evenement`
--

CREATE TABLE `Evenement` (
  `idE` int(12) NOT NULL,
  `titre` varchar(20) NOT NULL,
  `description` varchar(500) NOT NULL,
  `image` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Evenement`
--

INSERT INTO `Evenement` (`idE`, `titre`, `description`, `image`, `date`) VALUES
(1, 'event 1', 'ce jeux est trop bien', 'http://google.com', '2020-11-01'),
(2, 'event 2', 'ce aussi jeux est trop bien', 'http://google.com', '2021-01-01');

-- --------------------------------------------------------

--
-- Structure de la table `Journaliste`
--

CREATE TABLE `Journaliste` (
  `idJ` int(12) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mdp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Suivre`
--

CREATE TABLE `Suivre` (
  `idU` int(12) NOT NULL,
  `idE` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Suivre`
--

INSERT INTO `Suivre` (`idU`, `idE`) VALUES
(1, 1),
(1, 2),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idU` int(12) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mdp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idU`, `nom`, `prenom`, `email`, `mdp`) VALUES
(1, 'admin', 'admin', 'admin@admin', 'admin'),
(2, 'toto', 'toto', 't@t', 'toto');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Evenement`
--
ALTER TABLE `Evenement`
  ADD UNIQUE KEY `idE` (`idE`);

--
-- Index pour la table `Journaliste`
--
ALTER TABLE `Journaliste`
  ADD UNIQUE KEY `idJ` (`idJ`);

--
-- Index pour la table `Suivre`
--
ALTER TABLE `Suivre`
  ADD KEY `foreign_event` (`idE`),
  ADD KEY `idU` (`idU`,`idE`) USING BTREE;

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD UNIQUE KEY `idU` (`idU`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Evenement`
--
ALTER TABLE `Evenement`
  MODIFY `idE` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Journaliste`
--
ALTER TABLE `Journaliste`
  MODIFY `idJ` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idU` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Suivre`
--
ALTER TABLE `Suivre`
  ADD CONSTRAINT `foreign_event` FOREIGN KEY (`idE`) REFERENCES `Evenement` (`idE`),
  ADD CONSTRAINT `foreign_joueur` FOREIGN KEY (`idU`) REFERENCES `utilisateur` (`idU`);
