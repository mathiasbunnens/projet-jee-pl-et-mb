package junia.projetbl.web.dto;

import java.util.Map;

public class UserDTO {

    private long id;

    private String name;

    private String email;

    private String password;

    private Map<Long, String> games;

    private Map<Long, String> companies;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Long, String> getGames() {
        return games;
    }

    public void setGames(Map<Long, String> games) {
        this.games = games;
    }

    public Map<Long, String> getCompanies() {
        return companies;
    }

    public void setCompanies(Map<Long, String> companies) {
        this.companies = companies;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
