package junia.projetbl.web.dto;

import junia.projetbl.core.entity.Game;

import java.util.Date;
import java.util.Map;

public class ArticleDTO {

    private long id;

    private Date persistDate;

    private Date updateDate;

    private String title;

    private String text;

    private Map<Long, String> game;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getPersistDate() {
        return persistDate;
    }

    public void setPersistDate(Date persistDate) {
        this.persistDate = persistDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<Long, String> getGame() {
        return game;
    }

    public void setGame(Map<Long, String> game) {
        this.game = game;
    }
}
