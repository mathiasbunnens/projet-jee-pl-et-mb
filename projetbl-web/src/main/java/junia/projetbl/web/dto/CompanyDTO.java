package junia.projetbl.web.dto;

import junia.projetbl.core.entity.Game;
import junia.projetbl.core.entity.User;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class CompanyDTO {

    private long id;

    private Date persistDate;

    private Date updateDate;

    private String name;

    private String description;

    private String imageURL;

    private Map<Long, String> games;

    private List<User> users;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getPersistDate() {
        return persistDate;
    }

    public void setPersistDate(Date persistDate) {
        this.persistDate = persistDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Map<Long, String> getGames() {
        return games;
    }

    public void setGames(Map<Long, String> games) {
        this.games = games;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
