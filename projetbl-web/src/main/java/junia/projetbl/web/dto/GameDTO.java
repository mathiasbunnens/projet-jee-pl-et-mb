package junia.projetbl.web.dto;

import junia.projetbl.core.entity.Article;
import junia.projetbl.core.entity.Company;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class GameDTO {

    private long id;

    private Date persistDate;

    private Date updateDate;

    private String name;

    private String description;

    private String imageURL;

    private Map<Long, String> company;

    private Map<Long, String> articles;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getPersistDate() {
        return persistDate;
    }

    public void setPersistDate(Date persistDate) {
        this.persistDate = persistDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Map<Long, String> getCompany() {
        return company;
    }

    public void setCompany(Map<Long, String> company) {
        this.company = company;
    }

    public Map<Long, String> getArticles() {
        return articles;
    }

    public void setArticles(Map<Long, String> articles) {
        this.articles = articles;
    }
}
