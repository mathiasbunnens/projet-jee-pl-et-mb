package junia.projetbl.web.controller;

import junia.projetbl.core.dao.GameDAO;
import junia.projetbl.core.entity.Article;
import junia.projetbl.core.entity.Game;
import junia.projetbl.core.service.GameService;
import junia.projetbl.web.dto.CompanyDTO;
import junia.projetbl.web.dto.GameDTO;

import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
@Path("/games")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GameController implements RestController {

    private GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GET
    @Path("")
    public Map<Long, String> findAllGames(){
        return gameService.findAll().stream().collect(Collectors.toMap(Game::getId, Game::getName));
    }

    @GET
    @Path("/{gameId}")
    public GameDTO findGameDetails(@PathParam("gameId") long gameId){
        Optional<Game> game = gameService.findGameDetails(gameId);
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(gameId);
        if (game.isPresent()){
            gameDTO.setName(game.get().getName());
            Map<Long, String> company = new HashMap<Long, String>() {{
                put(game.get().getCompany().getId(), game.get().getCompany().getName());
            }};
            gameDTO.setCompany(company);
            gameDTO.setDescription(game.get().getDescription());
            gameDTO.setImageURL(game.get().getImageURL());
            gameDTO.setArticles(game.get().getArticles().stream().collect(Collectors.toMap(Article::getId, Article::getTitle, (value1, value2) -> {
                System.out.println("duplicate key found!");
                return value1;
            })));
        }
        return gameDTO;
    }
}
