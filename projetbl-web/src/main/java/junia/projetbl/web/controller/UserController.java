
package junia.projetbl.web.controller;

import junia.projetbl.core.entity.Article;
import junia.projetbl.core.entity.Company;
import junia.projetbl.core.entity.Game;
import junia.projetbl.core.entity.User;
import junia.projetbl.core.service.ArticleService;
import junia.projetbl.core.service.CompanyService;
import junia.projetbl.core.service.GameService;
import junia.projetbl.core.service.UserService;
import junia.projetbl.web.dto.ArticleDTO;
import junia.projetbl.web.dto.UserDTO;

import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.stream.Collectors;


@Named
@Path("/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController implements RestController {

    private UserService userService;
    private ArticleService articleService;
    private GameService gameService;
    private CompanyService companyService;

    public UserController(UserService userService, ArticleService articleService, GameService gameService, CompanyService companyService) {
        this.userService = userService;
        this.articleService = articleService;
        this.gameService = gameService;
        this.companyService = companyService;
    }

    @GET
    @Path("/{userId}")
    public UserDTO findUserDetails(@PathParam("userId") long userId) {
        Optional<User> user = userService.findUserDetails(userId);
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userId);
        if (user.isPresent()){
            userDTO.setName(user.get().getName());
            userDTO.setCompanies(user.get().getCompanies().stream().collect(Collectors.toMap(Company::getId, Company::getName)));
            userDTO.setGames(user.get().getGames().stream().collect(Collectors.toMap(Game::getId, Game::getName)));
        }
        return userDTO;
    }


    @GET
    @Path("/{userId}/news")
    public Set<ArticleDTO> findUserNews(@PathParam("userId") long userId){
        Optional<User> user = userService.findUserDetails(userId);
        Set<ArticleDTO> articlesDTO = new HashSet<>();
        if(user.isPresent()){
            Set<Game> allGames = new HashSet<>();
            Set<Company> userCompanies = user.get().getCompanies();
            Set<Game> userGames = user.get().getGames();

            for (Game game : userGames){
                allGames.add(game);
            }

            if(userCompanies != null){
                for (Company company : userCompanies) {
                    if(company.getGames() != null){
                        for (Game game : company.getGames()){
                            allGames.add(game);
                        }
                    }
                }
            }


            Set<Article> articles = new HashSet<>();
            for (Game game : allGames){
                articles.addAll(game.getArticles());
            }
            ArticleController articleController = new ArticleController(articleService);

            for (Article article : articles){
                articlesDTO.add(articleController.findArticleDetails(article.getId()));
            }

        }
        return articlesDTO;
    }

    @GET
    @Path("/connexion/{userEmail}")
    public UserDTO connectUser(@PathParam("userEmail") String userEmail){
        User user = userService.findUserByEmail(userEmail);
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(user.getEmail());
        userDTO.setId(user.getId());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }

    @POST
    @Path("/addgame/{userId}/{gameId}")
    public void followGame(@PathParam("userId") long userId, @PathParam("gameId") long gameId){
        Optional<User> user = userService.findUserDetails(userId);
        Set<Game> games = user.get().getGames();
        Optional<Game> newGame = gameService.findGameDetails(gameId);
        games.add(newGame.get());
        user.get().setGames(games);
        userService.save(user.get());
    }

    @POST
    @Path("/removegame/{userId}/{gameId}")
    public void unfollowGame(@PathParam("userId") long userId, @PathParam("gameId") long gameId){
        Optional<User> user = userService.findUserDetails(userId);
        Set<Game> games = user.get().getGames();
        Optional<Game> newGame = gameService.findGameDetails(gameId);
        //games.remove(newGame.get());

        Set<Game> newGames = new HashSet<>();
        for(Game game : games){
            if(game.getId() != newGame.get().getId()){
                newGames.add(game);
            }
        }

        user.get().setGames(newGames);
        userService.save(user.get());
    }



    @POST
    @Path("/addcompany/{userId}/{companyId}")
    public void followCompany(@PathParam("userId") long userId, @PathParam("companyId") long companyId){
        Optional<User> user = userService.findUserDetails(userId);
        Set<Company> companies = user.get().getCompanies();
        Optional<Company> newCompany = companyService.findCompanyDetails(companyId);
        companies.add(newCompany.get());
        user.get().setCompanies(companies);
        userService.save(user.get());
    }

    @POST
    @Path("/removecompany/{userId}/{companyId}")
    public void unfollowCompany(@PathParam("userId") long userId, @PathParam("companyId") long companyId){
        Optional<User> user = userService.findUserDetails(userId);
        System.out.println("\n\n\n\n\nremoving from user " + user.get().getId());
        Set<Company> companies = user.get().getCompanies();
        Optional<Company> newCompany = companyService.findCompanyDetails(companyId);
        System.out.println("\n\n\n\n\nremoving company " + newCompany.get().getId());
        companies.remove(newCompany.get());
        user.get().setCompanies(companies);
        System.out.println("\n\n\n\n\nnew company list : " + user.get().getCompanies());
        userService.save(user.get());
    }
}
