package junia.projetbl.web.controller;


import junia.projetbl.core.entity.Article;
import junia.projetbl.core.service.ArticleService;
import junia.projetbl.web.dto.ArticleDTO;

import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
@Path("/articles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ArticleController implements RestController {

    private ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GET
    @Path("")
    public Map<Long, String> findAllArticles(){
        return articleService.findAll().stream().collect(Collectors.toMap(Article::getId, Article::getTitle));
    }

    @GET
    @Path("/{articleId}")
    public ArticleDTO findArticleDetails(@PathParam("articleId") long articleId){
        Optional<Article> article = articleService.findArticleDetails(articleId);
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setId(articleId);
        if (article.isPresent()){
            Map<Long, String> game = new HashMap<Long, String>() {{
                put(article.get().getGame().getId(), article.get().getGame().getName());
            }};
            articleDTO.setGame(game);
            articleDTO.setTitle(article.get().getTitle());
            articleDTO.setText(article.get().getText());
        }

        return articleDTO;
    }


}
