package junia.projetbl.web.controller;


import junia.projetbl.core.entity.Company;
import junia.projetbl.core.entity.Game;
import junia.projetbl.core.service.CompanyService;
import junia.projetbl.web.dto.CompanyDTO;

import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
@Path("/companies")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CompanyController implements RestController {

    private CompanyService companyService;

    public CompanyController (CompanyService companyService) { this.companyService = companyService; }

    @GET
    @Path("")
    public Map<Long, String> findAllCompanies(){
        return companyService.findAll().stream().collect(Collectors.toMap(Company::getId, Company::getName));
    }

    @GET
    @Path("/{companyId}")
    public CompanyDTO findCompanyDetails(@PathParam("companyId") long companyId){
        Optional<Company> company = companyService.findCompanyDetails(companyId);
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setId(companyId);
        companyDTO.setName(company.get().getName());
        companyDTO.setDescription(company.get().getDescription());
        companyDTO.setImageURL(company.get().getImageURL());
        companyDTO.setGames(company.get().getGames().stream().collect(Collectors.toMap(Game::getId,
                Game::getName,
                (value1, value2) -> {
            System.out.println("duplicate key found!");
            return value1;
        })));
        return companyDTO;
    }

}
