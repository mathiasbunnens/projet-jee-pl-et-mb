
var app = angular.module('consumeRestApp', ['ngResource']);


app.factory("articles", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/articles");
});

app.controller("ArticlesCtrl", function($scope, articles) {
    articles.get(function(data) {
        $scope.articles = data;
        console.log(Object.keys(data))
    }, function(err) {
        console.error("Error occured: ", err);
    });
});



app.factory("article", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/articles/:articleId", {articleId: '@articleId'});
});

app.controller("ArticleCtrl", function($scope, article) {
    $scope.init = function(articleId){
        article.get({articleId: articleId}, function(data) {
            $scope.article = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});



app.factory("game", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/games/:gameId", {gameId: '@gameId'});
});

app.controller("ArticleGameCtrl", function($scope, game) {
    $scope.init = function(gameId){
        game.get({gameId: gameId}, function(data) {
            $scope.newsGame = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});


app.factory("company", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/companies/:companyId", {companyId: '@companyId'});
});

app.controller("ArticleCompanyCtrl", function($scope, company) {
    $scope.init = function(companyId){
        company.get({companyId: companyId}, function(data) {
            $scope.newsCompany = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});
