
var app = angular.module('consumeRestApp', ['ngResource'])

app.factory("games", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/games");
});

app.controller("GamesCtrl", function($scope, games) {
    games.get(function(data) {
        $scope.games = data;
        console.log(Object.keys(data))
    }, function(err) {
        console.error("Error occured: ", err);
    });
});



app.factory("game", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/games/:gameId", {gameId: '@gameId'});
});

app.controller("GameCtrl", function($scope, game) {
    $scope.init = function(gameId){
        game.get({gameId: gameId}, function(data) {
            $scope.game = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});