
var app = angular.module('consumeRestApp', ['ngResource']);


app.factory("companies", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/companies");
});

app.controller("CompaniesCtrl", function($scope, companies) {
    companies.get(function(data) {
        $scope.companies = data;
        console.log(Object.keys(data))
    }, function(err) {
        console.error("Error occured: ", err);
    });
});



app.factory("company", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/companies/:companyId", {companyId: '@companyId'});
});

app.controller("CompanyCtrl", function($scope, company) {
    $scope.init = function(companyId){
        company.get({companyId: companyId}, function(data) {
            $scope.company = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});
