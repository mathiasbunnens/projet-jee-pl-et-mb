
var dataApp = angular.module('consumeRestApp', ['ngResource']);

dataApp.config(['$locationProvider', function($locationProvider) {$locationProvider.html5Mode({enabled: true, requireBase: false}); }])

dataApp.factory("userData", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/user/:userId", {userId: "@userId"});
});

dataApp.controller("DataCtrl", function($scope, userData, $location) {
    $scope.init = function(){
        var id = $location.search().id
        console.log(id)
        console.log(Object.keys(id))
        userData.get({userId : id}, function(data) {
            $scope.userData = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    }

});



dataApp.factory("userNews", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/user/:userId/news", {userId: "@userId"});
});

dataApp.controller("NewsCtrl", function($scope, userNews, $location) {
    $scope.init = function(){
        var id = $location.search().id
        console.log(id)
        userNews.query({userId : id}, function(data) {
            $scope.userNews = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    }

});



dataApp.factory("companies", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/companies");
});

dataApp.controller("ProfilCompaniesCtrl", function($scope, companies) {
    companies.get(function(data) {
        $scope.profilCompanies = data;
        console.log(Object.keys(data))
    }, function(err) {
        console.error("Error occured: ", err);
    });
});



dataApp.factory("company", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/companies/:companyId", {companyId: '@companyId'});
});

dataApp.controller("ProfilCompanyCtrl", function($scope, company) {
    $scope.init = function(companyId){
        company.get({companyId: companyId}, function(data) {
            $scope.profilCompany = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});



dataApp.factory("games", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/games");
});

dataApp.controller("ProfilGamesCtrl", function($scope, games) {
    games.get(function(data) {
        $scope.profilGames = data;
        console.log(Object.keys(data))
    }, function(err) {
        console.error("Error occured: ", err);
    });
});



dataApp.factory("game", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/games/:gameId", {gameId: '@gameId'});
});

dataApp.controller("ProfilGameCtrl", function($scope, game) {
    $scope.init = function(gameId){
        game.get({gameId: gameId}, function(data) {
            $scope.profilGame = data;
            console.log(Object.keys(data))
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});



dataApp.controller("AddGameCtrl", function($scope, $http, $location, $window) {
    $scope.init = function(gameId){
        var userId = $location.search().id
        var url = "http://localhost:8080/projetbl/api/user/addgame/" + userId + "/" + gameId
        $http.post(url).then(function (response) {
        // This function handles success
            $window.location.reload()
        }, function (response) {
        // this function handles error

        });
    };
});

dataApp.controller("RemoveGameCtrl", function($scope, $http, $location, $window) {
    $scope.init = function(gameId){
        var userId = $location.search().id
        var url = "http://localhost:8080/projetbl/api/user/removegame/" + userId + "/" + gameId
        $http.post(url).then(function (response) {
            // This function handles success
            $window.location.reload()
        }, function (response) {
            // this function handles error

        });
    };
});

dataApp.controller("AddCompanyCtrl", function($scope, $http, $location, $window) {
    $scope.init = function(companyId){
        var userId = $location.search().id
        var url = "http://localhost:8080/projetbl/api/user/addcompany/" + userId + "/" + companyId
        $http.post(url).then(function (response) {
            // This function handles success
            $window.location.reload()
        }, function (response) {
            // this function handles error

        });
    };
});

dataApp.controller("RemoveCompanyCtrl", function($scope, $http, $location, $window) {
    $scope.init = function(companyId){
        var userId = $location.search().id
        var url = "http://localhost:8080/projetbl/api/user/removecompany/" + userId + "/" + companyId
        $http.post(url).then(function (response) {
            // This function handles success
            $window.location.reload()
        }, function (response) {
            // this function handles error

        });
    };
});