
var connexionApp = angular.module("connexionApp", ['ngResource']);


connexionApp.factory("connexion", function($resource) {
    return $resource("http://localhost:8080/projetbl/api/user/connexion/:userEmail", {userEmail: '@userEmail'});
});

connexionApp.controller("ConnexionCtrl", function($scope, connexion, $window) {
    $scope.init = function(userEmail, userPassword){
        connexion.get({userEmail: userEmail}, function(data) {
            $scope.connexion = data;
            console.log(Object.keys(data));
            console.log($scope.connexion.password)
            console.log(userPassword)
            if($scope.connexion.password === userPassword){
                $window.location.href = "http://localhost:8080/projetbl/Profil.html?id=" + $scope.connexion.id
            } else {
                $window.location.href = "http://localhost:8080/projetbl"
            }
        }, function(err) {
            console.error("Error occured: ", err);
        });
    };
});
