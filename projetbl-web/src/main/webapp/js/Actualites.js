window.onload = function () {
    let el = document.getElementsByClassName("bg")
    

    for (let i = 0; i < el.length; i++) {

        let height = el[i].clientHeight;
        let width = el[i].clientWidth;

        /* simulate mouse position */
        el[i].addEventListener('mousemove', function (e) {

            /* get mouse position */
            let xVal = e.layerX;
            let yVal = e.layerY;

            let yRotation = 5 * ((xVal - width / 2) / width);
            let xRotation = -5 * ((yVal - height / 2) / height);
            let string = 'perspective(1000px) scale(1.01) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)'

            /* transformation */
            el[i].style.transform = 'perspective(1000px) scale(1.01) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)';

        })

        /* simulate click */
        el[i].addEventListener('mousedown', function () {
            el[i].style.transform = "perspective(500px) scale(0.95) rotateX(2deg) rotateY(0)"
        });

        /* simulate release of mouse click */
        el[i].addEventListener('mouseup', function () {
            el[i].style.transform = "perspective(500px) scale(1.05) rotateX(2deg) rotateY(0)"
        });

    }




}



