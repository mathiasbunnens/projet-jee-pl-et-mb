package junia.projetbl.data;

import junia.projetbl.core.entity.*;
import junia.projetbl.core.service.*;
import org.hibernate.hql.internal.ast.HqlASTFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.*;
import java.util.concurrent.ConcurrentMap;

public class Application {

    public static void main(String[] args) {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("junia.projetbl.core.config");
        final CompanyService companyService = context.getBean(CompanyService.class);
        final ArticleService articleService = context.getBean(ArticleService.class);
        final GameService gameService = context.getBean(GameService.class);
        final UserService userService = context.getBean(UserService.class);
        cleanDB(companyService, articleService, gameService, userService);
        final Map<String, Company> companies = registerCompanies(companyService);
        final Map<String, Game> games = registerGames(gameService, companies);
        final Map<Integer, Article> articles = registerArticles(articleService, games);
        final Map<String, User> users = registerUsers(companies, games, userService);
    }

    private static void cleanDB(final CompanyService companyService, final ArticleService articleService, final GameService gameService,
            final UserService userService) {
        userService.deleteAll();
        articleService.deleteAll();
        gameService.deleteAll();
        companyService.deleteAll();
        //TODO Why repetition ?
        //userService.deleteAll(); //WHY REPETITION
    }

    private static Map<String, Company> registerCompanies(final CompanyService companyService) {
        Map<String, Company> companies = new HashMap<>();
        companies.put("nintendo", createCompany("Nintendo",
                "Nintendo est une entreprise multinationale japonaise fondée en 1889 par Fusajirō Yamauchi près de Kyoto au Japon. Elle est spécialisée dans la fabrication de consoles de jeu vidéo, comme la Nintendo Entertainment System (NES), la Nintendo 64, la Wii et la Nintendo DS, ainsi que dans la conception de jeux vidéo tels que Super Mario ou The Legend of Zelda.",
                "https://upload.wikimedia.org/wikipedia/commons/b/b3/Nintendo_red_logo.svg",
                companyService));
        companies.put("valve", createCompany("Valve Corporation",
                "Valve Corporation est un studio américain de développement de jeux vidéo basé à Bellevue, dans l'État de Washington. Valve est devenu célèbre après la sortie de son premier jeu, Half-Life, en novembre 1998. Le studio a prolongé le succès d'Half-Life en développant des mods, des spin-off et des jeux, parmi lesquels on compte Half-Life 2.",
                "https://upload.wikimedia.org/wikipedia/commons/4/48/Valve_old_logo.svg",
                companyService));
        companies.put("rockstar", createCompany("Rockstar Games",
                "Rockstar Games est un développeur et éditeur de jeux vidéo basé à New York aux États-Unis, appartenant à Take-Two Interactive depuis le rachat en 1998 de l'éditeur britannique de jeux vidéo BMG Interactive.\n" +
                        "\n" +
                        "La marque est principalement connue pour la série Grand Theft Auto, qui a pris une importance majeure dans le monde du jeu vidéo au cours des années 2000. L'entreprise détient d'autres franchises phares qui ont fait sa renommée, avec notamment Canis Canem Edit, Max Payne, Midnight Club, Manhunt ou encore la série Red Dead. La plupart des jeux de la marque se déroulent en monde ouvert.",
                "https://upload.wikimedia.org/wikipedia/commons/5/53/Rockstar_Games_Logo.svg",
                companyService));
        companies.put("ea", createCompany("Electronic Arts",
                "Electronic Arts ou EA (NASDAQ : EA [archive]) est une société américaine fondée le 28 mai 1982 et dont le siège se situe à Redwood City en Californie. EA est l'un des principaux développeurs et producteurs mondiaux de jeux vidéo.\n" +
                        "\n" +
                        "La société occupe la place de leader sur ce marché jusqu'en 2008, notamment grâce à des rachats de sociétés et de franchises de jeux, mais aussi en acquérant les droits de licences sportives, comme celles de la FIFA, la NBA, la NFL, ou encore celle de la LNH.",
                "https://upload.wikimedia.org/wikipedia/commons/8/81/Electronic_Arts_2020.svg",
                companyService));
        companies.put("actiblizz", createCompany("Activision Blizzard",
                "Activision Blizzard, Inc. est un développeur et éditeur américain de jeux vidéo. La firme est le résultat de la fusion entre Activision et Vivendi Games, annoncée en décembre 2007 et finalisée en juillet 2008.\n" +
                        "\n" +
                        "Activision Blizzard est en 2012 la plus grande société commercialisant des jeux vidéo par chiffre d'affaires. En juillet 2013, l'actionnaire majoritaire Vivendi cède la plupart de ses actions à des actionnaires minoritaires du groupe, faisant d'Activision Blizzard une compagnie indépendante intégralement contrôlée par Robert Kotick.",
                "https://upload.wikimedia.org/wikipedia/commons/6/6e/Activision_Blizzard_logo.svg",
                companyService));
        return companies;
    }

    private static Company createCompany(String companyName, String companyDescription, String companyImageURL, final CompanyService companyService) {
        System.out.println("Registering company " + companyName);
        Company company = new Company();
        company.setName(companyName);
        company.setDescription(companyDescription);
        company.setImageURL(companyImageURL);
        companyService.save(company);
        return company;
    }

    private static Map<String, Game> registerGames(final GameService gameService, final Map<String, Company> companies) {
        Map<String, Game> types = new HashMap<>();
        types.put("gta5", createGame("Grand Theft Auto V",
                "Grand Theft Auto V (plus communément abrégé GTA V) est un jeu vidéo d'action-aventure, développé par Rockstar North et édité par Rockstar Games en 2013. Faisant partie de la série vidéoludique série des jeux vidéo Grand Theft Auto, il est une suite de l'univers fictif introduit dans Grand Theft Auto IV, sorti en 2008. Quinzième jeu de la série en comptant les contenus additionnels et les épisodes portables, GTA V est le cinquième volet officiel de la saga ainsi que le seul opus de la franchise à sortir sur trois générations de consoles successives, à savoir la septième, la huitième et la neuvième génération (GTA 2 est sorti lui à cheval sur deux générations de consoles, celles de la cinquième et de la sixième génération).",
                "https://upload.wikimedia.org/wikipedia/fr/d/d5/Grand_Theft_Auto_V_Logo.svg",
                companies.get("rockstar"),
                gameService));
        types.put("reddeadr2", createGame("Red Dead Redemption 2",
                "Red Dead Redemption II est un jeu vidéo d'action-aventure et de western multiplateforme, développé par Rockstar Studios et édité par Rockstar Games, sorti le 26 octobre 2018 sur PlayStation 4 et Xbox One3 et le 5 novembre 2019 sur Windows. Une version Stadia est également sortie le 19 novembre 2019.",
                "https://upload.wikimedia.org/wikipedia/fr/9/99/Red_Dead_Redemption_2_Logo_Final.png",
                companies.get("rockstar"),
                gameService));
        types.put("tetris", createGame("Tetris",
                "Tetris est un jeu vidéo de puzzle conçu par Alekseï Pajitnov à partir de juin 1984 sur Elektronika 60. Lors de la création du concept, Pajitnov est aidé de Dmitri Pavlovski et Vadim Guerassimov pour le développement. Le jeu est édité par plusieurs sociétés au cours du temps, à la suite d'une guerre pour l'appropriation des droits à la fin des années 1980. Le déroulement précis du développement et des premières commercialisations est encore débattu dans les années 2010. Après une exploitation importante par Nintendo, les droits appartiennent depuis 1996 à la société The Tetris Company. Cette société a vendu les droits d'exploitation sur mobiles à Electronic Arts et sur consoles à Ubisoft.",
                "https://upload.wikimedia.org/wikipedia/fr/d/d4/The_Tetris_Company_Logo.png",
                companies.get("ea"),
                gameService));
        types.put("fifa2021", createGame("Fifa 2021",
                "FIFA 21 est un jeu vidéo de football développé par EA Canada et EA Roumanie et édité par EA Sports. La date de sortie du jeu, annoncée le 18 juin 2020, est prévue le 9 octobre 2020 sur PC, PlayStation 4, Xbox One et Nintendo Switch et plus tard sur PlayStation 5, Xbox Series et Stadia. Le jeu est également disponible à partir du 1er octobre pour les joueurs bénéficiant de l'EA Access ou de l'Origin Access. Trois versions différentes de cet opus sont disponibles : l'édition Standard, l'édition Champions et l'édition Ultimate. Il s'agit du vingt-huitième opus de la franchise FIFA développé par EA Sports.",
                "https://upload.wikimedia.org/wikipedia/commons/f/f6/FIFA_21_logo.svg",
                companies.get("ea"),
                gameService));
        types.put("apexlegends", createGame("Apex Legends",
                "Apex Legends est un jeu vidéo de type battle royale développé par Respawn Entertainment et édité par Electronic Arts. Il est publié en accès gratuit le 4 février 2019 sur Microsoft Windows, PlayStation 4 et Xbox One.\n" +
                        "\n" +
                        "Aussitôt considéré comme un concurrent du très populaire Fortnite Battle Royale, le jeu bat des records de fréquentation dès sa sortie, en réunissant plus de 2,5 millions de joueurs en 24 heures, 10 millions en trois jours puis 25 millions en une semaine après sa commercialisation. Après un mois de commercialisation, le jeu dépasse la barre des 50 millions de joueurs. Le déploiement sur mobile est prévu d'ici fin 2020, dans le but de toucher un plus large public.",
                "https://upload.wikimedia.org/wikipedia/fr/0/03/Apex_Legends_Logo.png",
                companies.get("ea"),
                gameService));
        types.put("battlefield5", createGame("Battlefield V",
                "Battlefield V (BFV) est un jeu vidéo de tir à la première personne développé par DICE et édité par Electronic Arts, sorti le 20 novembre 2018 sur PlayStation 4, Xbox One et Microsoft Windows. Ce douzième opus de la série se déroule dans le contexte de la Seconde Guerre Mondiale, revenant aux origines de la série.",
                "https://upload.wikimedia.org/wikipedia/commons/9/9c/Battlefield_v_official_logo.png",
                companies.get("ea"),
                gameService));
        types.put("wiisports", createGame("Wii Sports",
                "Wii Sports est un jeu vidéo de sport développé et édité par Nintendo comme titre de lancement pour la console de jeux vidéo Wii. Il est commercialisé dans un premier temps en Amérique du Nord le 19 novembre 2007, et sort le mois suivant au Japon, en Australie et en Europe. Le jeu est inclus dans un paquetage promotionnel avec la Wii sur tous les territoires, excepté au Japon et en Corée du Sud, faisant de lui le premier jeu vendu avec une console Nintendo lors de sa sortie depuis Super Mario World sur Super Nintendo en 1990. Wii Sports a entamé la série de jeux Wii de Nintendo.",
                "https://upload.wikimedia.org/wikipedia/fr/7/78/Wii_Sports_Logo.svg",
                companies.get("nintendo"),
                gameService));
        types.put("diablo3", createGame("Diablo III",
                "Diablo III est un jeu vidéo d'action et de rôle de type hack and slash développé par Blizzard Entertainment. Il constitue le troisième opus de la série, succédant à Diablo et à Diablo II. Publié par Activision Blizzard, le jeu a bénéficié d'une sortie mondiale le 15 mai 2012. Comme ses prédécesseurs, il se déroule dans un monde imaginaire de type médiéval-fantastique dans lequel le joueur peut choisir d’incarner un sorcier, un barbare, un féticheur, un chasseur de démon ou un moine. À sa sortie, il est bien accueilli par les critiques et il connaît un important succès commercial. Le jeu s’écoule ainsi à 4,7 millions d’exemplaires le jour de sa sortie, ce qui constitue un record pour l’époque, et il a depuis dépassé les quinze millions de titres vendus. Depuis le 3 septembre 2013, le jeu est également disponible sur PlayStation 3 et Xbox 360.",
                "https://upload.wikimedia.org/wikipedia/fr/5/5d/Diablo_III_Logo.jpg",
                companies.get("actiblizz"),
                gameService));
        types.put("worldofwarcraft", createGame("World of Warcraft",
                "World of Warcraft (abrégé WoW) est un jeu vidéo de type MMORPG (jeu de rôle en ligne massivement multijoueur) développé par la société Blizzard Entertainment. C'est le 4e jeu de l'univers médiéval-fantastique Warcraft, introduit par Warcraft: Orcs and Humans en 1994. World of Warcraft prend place en Azeroth, près de quatre ans après les événements de la fin du jeu précédent, Warcraft III: The Frozen Throne (2003). Blizzard Entertainment annonce World of Warcraft le 2 septembre 2001. Le jeu est sorti en Amérique du Nord le 23 novembre 2004, pour les 10 ans de la franchise Warcraft.",
                "https://upload.wikimedia.org/wikipedia/fr/e/e3/World_of_Warcraft_Logo.png",
                companies.get("actiblizz"),
                gameService));
        types.put("overwatch", createGame("Overwatch",
                "Overwatch est un jeu vidéo de tir en vue subjective, en équipes, en ligne, développé et publié par Blizzard Entertainment. Le jeu est annoncé le 7 novembre 2014 à la BlizzCon, et est commercialisé le 24 mai 2016 sur Windows, PlayStation 4 et Xbox One et le 15 octobre 2019 sur Nintendo Switch. Le jeu met l'accent sur la coopération entre différentes classes représentées par différents personnages ayant chacun leurs capacités et particularités. Le jeu s'inspire notamment des jeux de tir en vue subjective en équipe de la décennie précédente mettant eux aussi l'accent sur la coopération entre plusieurs classes de personnage, notamment Team Fortress 2.",
                "https://upload.wikimedia.org/wikipedia/fr/d/d9/Overwatch_Logo.png",
                companies.get("actiblizz"),
                gameService));
        types.put("halflife", createGame("Half-Life",
                "Half-Life, stylisé HλLF-LIFE, est une série de jeux vidéo de tir à la première personne de science-fiction. La série doit son nom au mot anglais signifiant demi-vie qui fait référence à la radioactivité.",
                "https://upload.wikimedia.org/wikipedia/commons/8/8f/Orange_lambda.svg",
                companies.get("valve"),
                gameService));
        types.put("dota2", createGame("Dota 2",
                "Dota 2 est un jeu vidéo de type arène de bataille en ligne multijoueur développé et édité par Valve Corporation avec l'aide de certains des créateurs du jeu d'origine : Defense of the Ancients, un mod de carte personnalisée pour le jeu de stratégie en temps réel Warcraft III: Reign of Chaos et son extension Warcraft III: The Frozen Throne. Le jeu est sorti en juillet 2013 sur Microsoft Windows, OS X et Linux mettant fin à une phase bêta commencée en 2011. Il est disponible exclusivement sur la plateforme de jeu en ligne Steam.",
                "https://upload.wikimedia.org/wikipedia/fr/f/f7/Dota_2_Logo.jpg",
                companies.get("valve"),
                gameService));
        types.put("halfLifealyx", createGame("Half-Life: Alyx",
                "Half-Life: Alyx est un jeu vidéo de tir à la première personne jouable uniquement en réalité virtuelle, développé et édité par Valve. Dernier jeu de la série Half-life depuis Half-Life 2: Episode Two publié en 2007, il est sorti sur Windows le 23 mars 2020. Le joueur y incarne Alyx Vance, fille du Docteur Eli Vance dans une période située entre Half-Life et Half-Life 2.",
                "https://upload.wikimedia.org/wikipedia/fr/e/e3/Half-Life_Alyx_Logo.png",
                companies.get("valve"),
                gameService));
        return types;
    }

    private static Game createGame(String name, String description, String imageURL, Company company, final GameService gameService) {
        System.out.println("Registering game " + name);
        Game game = new Game();
        game.setName(name);
        game.setDescription(description);
        game.setImageURL(imageURL);
        game.setCompany(company);
        gameService.save(game);
        return game;
    }

    private static Map<Integer, Article> registerArticles(final ArticleService articleService, final Map<String, Game> games) {
        Map<Integer, Article> articles = new HashMap<>();
        articles.put(1, createArticle("Une vrai Rock Star",
                "Rarement un jeu aura autant cristallisé les passions que Grand Theft Auto V. D'un côté la flamme des joueurs, sevrés de \"vrai\" GTA depuis plusieurs années, malgré les prestations très honorables de la concurrence. De l'autre la poudre aux yeux d'une machine marketing parfaitement huilée, jusqu'à contrôler la moindre parcelle d'information comme si elle était classée Secret Défense.\n" +
                        "\n" +
                        "Ajoutez-y quelques étincelles (\"le jeu le plus cher du monde\", \"de vrais gangsters au doublage\"), et vous obtenez le cocktail le plus détonnant depuis qu'un certain Molotov s'est penché sur le sujet, avec au terme d'une attente presque déraisonnable, des files de clients que n'auraient pas reniées l'Union Soviétique et un Rockstar qui impose une pression monstre à ceux qui oseraient braver son autorité. A quoi bon : l'heure n'est plus aux fantasmes ni même aux caprices de diva. Après plusieurs mois d'attente, on sait enfin ce que vaut le dernier GTA de sa génération.",
                games.get("gta5"),
                articleService));
        articles.put(2, createArticle("Testé pour Nintendo 3DS",
                "Nous ne vous ferons pas ici l'outrage de rappeler le concept de Tetris ; cela fait plus de vingt-cinq ans que l'on s'emboîte gaiement en évitant de voir la cuve déborder. Après une version DS soignée sous tous rapports, on retrouve l'icône du jeu vidéo dans une adaptation 3DS beaucoup moins tantrique.",
                games.get("tetris"),
                articleService));
        articles.put(3, createArticle("Wii Sports : toutouyoutou",
                "Dans l'ensemble, Wii Sports reste malgré ses très nombreuses limites le parfait moyen de réunir rapidement toutes sortes de personnes autour de la Wii, et Nintendo a donc vu juste à ce niveau. Il ne fait aucun doute que les enfants qui déballeront leur console à Noël n'auront pas trop de mal à faire jouer tata Micheline ou papi René le temps de quelques parties sympa. Le même type de personnes qui n'auraient jamais osé (et surtout voulu) attraper une manette pour tenter quoique ce soit dans un Splinter Cell ou un Dragon Ball Z, en fait. Certains seront donc ravis de trouver Wii Sports au moment d'ouvrir le carton de leur Wii flambante neuve, mais il va sans dire que les joueurs confirmés auraient aussi pu s'en passer. Accessible mais aussi vite limité d'un point de vue de gamer, Wii Sports servira essentiellement à s'amuser de temps en temps avec des amis novices qui se désintéressent des jeux vidéo classiques. C'est déjà ça de pris, et les possibilités de la Wii ont le mérite d'être démontrées par la pratique. Mais quitte à choisir, il ne fait nul doute que les joueurs confirmés auraient préféré avoir le choix d'acheter la console nue à un tarif plus doux plutôt que d'être contraints de payer pour un Wii Sports obligatoire par console. La conquête du grand public est à ce prix.",
                games.get("wiisports"),
                articleService));
        articles.put(4, createArticle("Test : Diablo III (PC)",
                "A une époque où les suites de grosses franchises sont déployées en quelques mois à peine et où les serveurs en ligne des anciennes versions sont discrètement fermés pour faire place à la nouvelle vague, Diablo II fait un peu office d'énorme exception avec ses douze ans de règne sur la toile. S'il est toujours plus facile de trouver une partie sur le légendaire hack and slash que sur les trois quarts de jeux \"triple A\" sortis ces dernières années, il était tout de même temps de passer le flambeau avec un nouvel épisode toujours signé Blizzard Entertainment. Tour à tour demandé, conspué, supplié ou critiqué, Diablo III est bel et bien sorti cette semaine et il est temps de vous donner notre avis sur ce troisième épisode de l'une des franchises les plus puissantes du jeu vidéo.",
                games.get("diablo3"),
                articleService));
        articles.put(5, createArticle("Half-Life Alyx est un sublime retour au coeur de cité 17",
                "Treize ans. Treize ans qu'une armée d'illuminés du pied-de-biche se retournent la tête pour essayer de se convaincre que Valve sait bel et bien compter jusqu'à trois. Des années de conspiration, de propos recueillis puis déformés, d'analyse poussée des claquettes de Gabe Newell ou de leaks en tout genre... Mais celle-là, vraiment, personne ne l'avait vue venir. Personne n'avait vu venir que treize ans après la sortie du dernier épisode, et le départ d'une bonne partie de ses scénaristes emblématiques, Valve sortirait un nouvel épisode de Half-Life en 2020. Un épisode exclusivement VR, qui plus est. Ce n'est pas Half-Life 3, mais pourtant il est bien là : Half-Life Alyx vient de sortir. Bien loin du jeu-friandise ou d'une simple expérience de réalité virtuelle, Half-Life Alyx est un vrai jeu, dense et complet, qui marque enfin notre retour à Cité 17. Et purée, quel grand retour.",
                games.get("halflife"),
                articleService));
        articles.put(6, createArticle("Apex Legends est une vision digéré du battle royale qui fait beaucoup de bien au genre",
                "Il y a bien longtemps qu'Electronic Arts n'avait pas signé un lancement aussi propre que celui d'Apex Legends, le battle royale annoncé (puis sorti dans la foulée) par surprise ce lundi 4 février. Aucune douille sur le modèle économique, presque pas de soucis de connexion, peu de soucis techniques à déplorer et ce malgré la percée du jeu sur Twitch et le million de joueurs qui s'est précipité sur les serveurs en l'espace de huit heures. Pourtant, à première vue, avec son pitch de battle royale free-to-play à héros et à lootboxes, Apex Legends coche toutes les cases du osef-o-mètre (marque déposée Gamekult™). Toutes, à une nuance près : il nous vient de Respawn Entertainement, les créateurs de la série Titanfall.",
                games.get("apexlegends"),
                articleService));
        return articles;
    }

    private static Article createArticle(String title, String text, final Game game, final ArticleService articleService) {
        System.out.println("Registering article " + title);
        Article article = new Article();
        article.setTitle(title);
        article.setText(text);
        article.setGame(game);
        articleService.save(article);
        return article;
    }

    private static Map<String, User> registerUsers(final Map<String, Company> companies, final Map<String, Game> games, final UserService userService) {
        Map<String, User> users = new HashMap<>();
        HashSet<Company> companiesSet = new HashSet<>();
        HashSet<Game> gamesSet = new HashSet<>();
        companiesSet.add(companies.get("nintendo"));
        gamesSet.add(games.get("tetris"));
        gamesSet.add(games.get("diablo3"));
        users.put("toto", createUser("toto",
                "toto@toto",
                "toto",
                companiesSet,
                gamesSet,
                userService));
        companiesSet.clear();
        gamesSet.clear();
        companiesSet.add(companies.get("rockstar"));
        gamesSet.add(games.get("gta5"));
        users.put("Daniel", createUser("Daniel",
                "Daniel@Daniel",
                "Daniel",
                companiesSet,
                gamesSet,
                userService));
        return users;
    }

    private static User createUser(String name, String email, String password, Set<Company> companies, Set<Game> games, final UserService userService) {
        System.out.println("Registering user " + name);
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        user.setCompanies(companies);
        user.setGames(games);
        userService.save(user);
        return user;
    }
}
